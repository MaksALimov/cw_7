import React from 'react';

const AddItemsButton = ({addItems, changeOrder}) => {
    return (
        <div>
            {addItems.map((food, i) => {
                return <button onClick={() => changeOrder(food.foodName, food.price, food.id, food.counter)}
                               key={i}>
                    <p>{food.foodName}</p>
                    <p>Price: {food.price}</p>
                    <img src={food.img} alt="food"/>
                    <p>X{food.counter}</p>
                </button>
            })}
        </div>
    );
};

export default AddItemsButton;