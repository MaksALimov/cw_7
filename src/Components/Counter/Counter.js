import React from 'react';

const Counter = ({addItems}) => {
    return (
        <div>
            {addItems.reduce((total, amount) => {
                return total + amount.counter * amount.price
            }, 0)}
        </div>
    );
};

export default Counter;