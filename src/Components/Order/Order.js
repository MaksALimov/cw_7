import React from 'react';
import Counter from "../Counter/Counter";

const Order = ({order, addItems, deleteOrder}) => {
    return (
        <div>
            {order.map((orders, i) => {
                return <div
                    key={i}>
                    <p>{orders.orderState}</p>
                    <p>{orders.foodName}</p>
                    <p>{orders.price}</p>
                    <Counter addItems={addItems}/>
                    <button onClick={() => deleteOrder(order.id)}>Delete</button>
                </div>
            })}
        </div>
    );
};

export default Order;