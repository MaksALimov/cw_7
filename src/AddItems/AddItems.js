import React from 'react';

const AddItems = ({changeOrder, food, i}) => {
    return (
        <div>
            <button onClick={() => changeOrder(food.foodName, food.price, food.id, food.counter)}
                    key={i}>
                <p>{food.foodName}</p>
                <p>Price: {food.price}</p>
                <img src={food.img} alt="food"/>
                <p>X{food.counter}</p>
            </button>
            })}
        </div>
    );
};

export default AddItems;