import {useState} from "react";
import Utensils from './assets/fork-knife.svg';
import Tea from './assets/tea.svg';
import Coffee from './assets/tea.svg';
import Cola from './assets/cola.svg'
import {nanoid} from "nanoid";
import './App.css';
import Order from "./Components/Order/Order";
import AddItemsButton from "./Components/AddItems/addItemsButton";

const App = () => {
    const [order, setOrder] = useState([
        {orderState: 'Order is empty'}
    ]);

    const [addItems, setAddItems] = useState([
        {foodName: 'Hamburger', price: 80, counter: 0, id: nanoid(), img: Utensils},
        {foodName: 'CheeseBurger', price: 90, counter: 0, id: nanoid(), img: Utensils},
        {foodName: 'Fries', price: 45, counter: 0, id: nanoid(), img: Utensils},
        {foodName: 'Coffee', price: 70, counter: 0, id: nanoid(), img: Coffee},
        {foodName: 'Tea', price: 50, counter: 0, id: nanoid(), img: Tea},
        {foodName: 'Cola', price: 40, counter: 0, id: nanoid(), img: Cola},
    ]);

    const changeOrder = (foodName, price, id, counter) => {
        setOrder(prev => [...prev, {foodName, price, id, counter}]);
        setAddItems(addItems.map(addItems => {
            if (addItems.id === id) {
                return {...addItems, counter: addItems.counter + 1};
            }

            return addItems;
        }));
    };

    const deleteOrder = id => {
        const orderCopy = [...order];
        const index = orderCopy.findIndex(order => order.id !== id);
        orderCopy.splice(index, 1);
        setOrder(orderCopy);
    };

    return (
        <div className="Wrapper">
            <fieldset>
                <legend>Order Details</legend>
                <Order order={order} addItems={addItems} deleteOrder={deleteOrder}/>
            </fieldset>
            <fieldset>
                <legend>Add Items</legend>
                <AddItemsButton addItems={addItems} changeOrder={changeOrder}/>
            </fieldset>
        </div>
    )
}
export default App;